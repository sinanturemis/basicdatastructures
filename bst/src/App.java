public class App {
    public static void main(String[] args) throws Exception {
        Bst btree = new Bst(10);
        btree.insert(5);
        btree.insert(15);
        btree.insert(8);
        btree.insert(7);
        btree.insert(9);

        btree.printInOrder();
    }
}
