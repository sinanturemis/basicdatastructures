public class Bst {
    Bst left;
    Bst right;
    public int val;

    public Bst(int val) {
        this.val = val;
    }

    void insert(int data) {
        if (data > this.val) {
            if (this.right == null) {
                this.right = new Bst(data);
            } else {
                this.right.insert(data);
            }
        } else {
            if (this.left == null) {
                this.left = new Bst(data);
            } else {
                this.left.insert(data);
            }
        }
    }

    boolean contains(int data) {
        if (this.val == data) {
            return true;
        }

        if (data > this.val) {
            if (this.right == null) {
                return false;
            }
            return this.right.contains(data);
        } else {
            if (this.left == null) {
                return false;
            }
            return this.left.contains(data);
        }
    }

    void printInOrder() {
        if (this.left != null) {
            this.left.printInOrder();
        }

        System.out.println(this.val);

        if (this.right != null) {
            this.right.printInOrder();
        }
    }

}
