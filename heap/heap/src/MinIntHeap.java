import java.util.List;

public class MinIntHeap {
    private List<Integer> array;

    private int getLeftChildIndex(int parent) {
        return 2 * parent + 1;
    }

    private int getRightChildIndex(int parent) {
        return 2 * parent + 2;
    }

    private int getParentIndex(int child) {
        return (child - 1) / 2;
    }

    private boolean hasLeftChild(int index) {
        return getLeftChildIndex(index) < array.size();
    }

    private boolean hasRightChild(int index) {
        return getRightChildIndex(index) < array.size();
    }

    private boolean hasParent(int index) {
        return getParentIndex(index) >= 0;
    }

    private int getLeftChild(int index) {
        return array.get(getLeftChildIndex(index));
    }

    private int getRightChild(int index) {
        return array.get(getRightChildIndex(index));
    }

    private int getParent(int index) {
        return array.get(getParent(index));
    }

    private void swap(int indexOne, int indexTwo) {
        int temp = array.get(indexOne);
        array.set(indexOne, array.get(indexTwo));
        array.set(indexTwo, temp);
    }

    public int poll() {
        if (array.size() == 0) {
            throw new IllegalStateException();
        }
        int item = array.get(0);

        swap(0, array.size() - 1);
        array.remove(array.size() - 1);

        heapifyDown();
        return item;
    }

    public void add(int val) {
        array.add(val);
        heapifyUp();
    }

    private void heapifyUp() {
        int index = array.size() - 1;
        while (hasParent(index) && getParent(index) > array.get(index)) {
            swap(getParentIndex(index), index);
            index = getParentIndex(index);

        }
    }

    private void heapifyDown() {
        int index = 0;
        while (hasLeftChild(index)) {
            int smallerIndex = getLeftChildIndex(index);
            if (hasRightChild(index) && getLeftChild(index) > getRightChild(index)) {
                smallerIndex = getRightChildIndex(index);
            }

            if (array.get(index) > array.get(smallerIndex)) {
                swap(index, smallerIndex);
                index = smallerIndex;
            } else {
                break;
            }
        }
    }

}