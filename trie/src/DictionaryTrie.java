public class DictionaryTrie {

    Node head;

    public DictionaryTrie() {
        head = new Node();
    }

    // apple
    public void add(String word) {
        Node current = head;
        for (int i = 0; i < word.length(); i++) {
            char currentChar = word.charAt(i);
            if (current.children[currentChar - 'a'] == null) {
                current.children[currentChar - 'a'] = new Node(currentChar);
            }

            current = current.children[currentChar - 'a'];
        }

        current.isWord = true;
    }

    public boolean isExist(String word) {
        Node endingNode = getEndingNode(word);
        return endingNode != null && endingNode.isWord;
    }

    public boolean startsWith(String prefix) {
        return getEndingNode(prefix) != null;
    }

    private Node getEndingNode(String word) {
        Node current = head;
        for (int i = 0; i < word.length(); i++) {
            char currentChar = word.charAt(i);
            if (current.children[currentChar - 'a'] == null) {
                return null;
            }

            current = current.children[currentChar - 'a'];
        }

        return current;
    }

    class Node {
        char value;
        boolean isWord;
        Node[] children;

        public Node(char val) {
            this.value = val;
            this.isWord = false;
            this.children = new Node[26];
        }

        public Node() {
            this.isWord = false;
            this.children = new Node[26];
        }
    }
}
